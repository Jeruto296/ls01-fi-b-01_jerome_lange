import java.util.Scanner;
public class PCHaendler {
	public static void main(String[] args) {
		double anzahl=0, preis=0, mwst=0;
		String artikel;
		artikel=eingabeString("Was m�chten Sie bestellen?");
		anzahl=eingabeZahl("Geben Sie die Anzahl ein: ");
		preis=eingabeZahl("Geben Sie den Preis ein: ");
		mwst=eingabeZahl("Geben Sie die mwst ein: ");
		verarbeitung(anzahl,preis,mwst);
		ausgabe(artikel,anzahl,preis,mwst);
	}
	public static String eingabeString(String Text) {
		Scanner sc=new Scanner(System.in);
		System.out.println(Text);
		String artikel= sc.nextLine(); 
		return artikel;
	}
	public static double eingabeZahl(String Text) {
		Scanner sc=new Scanner(System.in);
		System.out.println(Text);
		return sc.nextDouble();
	}
	public static double verarbeitung(double anzahl,double preis,double mwst) {
		
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	public static void ausgabe(String artikel,double anzahl,double preis,double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6.0f %10.2f %n", artikel, anzahl, anzahl * preis);
		System.out.printf("\t\t Brutto: %-20s %6.0f %10.2f (%.1f%s)%n", artikel, anzahl, verarbeitung(anzahl, preis, mwst), mwst, "%");
	}
}