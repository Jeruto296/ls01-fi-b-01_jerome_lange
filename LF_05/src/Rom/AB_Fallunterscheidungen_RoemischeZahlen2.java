// Die r�mischen Zahlen werden berechnet und auf Korrektheit gepr�ft.
// Dies geschieht hinsichtlich der Position der Ziffern und deren Anzahl.

package Rom;

import java.util.Scanner;

public class AB_Fallunterscheidungen_RoemischeZahlen2 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine r�mische Zahl ein (1 - 3999): ");
		String roemisch = scanner.next();

		// Counter Marken zur Fehlerpr�fung
		int mCounter = 0;
		int dCounter = 0;
		int cdOrcmCounter = 0;
		int cCounter = 0;
		int lCounter = 0;
		int xcOrxlCounter = 0;
		int xCounter = 0;
		int vCounter = 0; 
		int iCounter = 0;
		int ixcOrivCounter = 0;
		
		int decimal = 0;
		
		
		boolean isReal = true;
		boolean error = false;

		for (int i = 0; i < roemisch.length(); i++) {
			// System.out.println(roemisch.charAt(i));
			switch (roemisch.charAt(i)) {

			
			case 'M': // 1000
				decimal += 1000;
				mCounter++; // Anzahlpr�fung
				if (iCounter > 0 || vCounter > 0 || xCounter > 0 || xcOrxlCounter > 0 || lCounter > 0 || cCounter > 1 || cdOrcmCounter > 1 || dCounter > 0 || mCounter > 3) error = true; 

				break;

				
			case 'D': // 500
				decimal += 500;
				dCounter++; // Anzahlpr�fung
				if (iCounter > 0 || vCounter > 0 || xCounter > 0 || xcOrxlCounter > 0 || lCounter > 0 || cCounter > 1 || cdOrcmCounter > 1 || dCounter > 1) error = true; 
				break;

				
			case 'C': // 100
				if (i + 1 < roemisch.length() && ( roemisch.charAt(i + 1) == 'D' || roemisch.charAt(i + 1) == 'M')) { 
					decimal -= 100;
					cdOrcmCounter++; // Anzahlpr�fung
					if (iCounter > 0 || vCounter > 0 || xCounter > 1 || xcOrxlCounter > 0 || lCounter > 0 || cCounter > 3 || cdOrcmCounter > 1) error = true; 
				} 
			
				else {
					decimal += 100;
					cCounter++; // Anzahlpr�fung
					if (iCounter > 0 || vCounter > 0 || xCounter > 1 || xcOrxlCounter > 0 || lCounter > 0 || cCounter > 3) error = true; 
					
				}
				break;

				
			case 'L': // 50
				decimal += 50;
				lCounter++; // Anzahlpr�fung
				if (iCounter > 0 || vCounter > 0 || xCounter > 1 || xcOrxlCounter > 0 || lCounter > 1) error = true; 
				break;
				
				
			case 'X': // 10
				if ( i + 1 < roemisch.length() && ( roemisch.charAt(i + 1) == 'C' || roemisch.charAt(i + 1) == 'L')) {
					decimal -= 10;
					xcOrxlCounter++; // Anzahlpr�fung
					if (iCounter > 1 || vCounter > 0 || xCounter > 3 || xcOrxlCounter > 1 ) error = true; // Links von X, d�rfen nicht mehr als 1 I, kein V und h�chstens 2 X
				}
				else {
					decimal += 10;
					xCounter++; // Anzahlpr�fung
					if (iCounter > 0 || vCounter > 0 || xCounter > 3) error = true; // Links von X, d�rfen nicht mehr als 0 I, kein V und h�chstens 2 X
				}
				break;
				
				
			case 'V': // 5
				decimal += 5;
				vCounter++; // Anzahlpr�fung
				if (iCounter>1 || vCounter>1) error = true; // Pr�ft Position
				break;
				
				
			case 'I':
				if (i + 1 < roemisch.length() && (roemisch.charAt(i + 1) == 'X' || roemisch.charAt(i + 1) == 'V')) {
					decimal -= 1;
					ixcOrivCounter++; 
					if (iCounter>0 || vCounter>0 || ixcOrivCounter>1) error = true; // Pr�ft ob I, V oder IV oder IX vor IV oder IX verwendet wurden, dass bedeutet eine Falscheingabe
					// System.out.println(vCounter +  " " + ixcOrivCounter);
				} 
				else {
					decimal += 1;
					iCounter++; 
					if (iCounter > 3) error = true; // Pr�ft ob I fehlerhafterweise viermal oder mehr verwendet wurde.
				}
				break;
				
				
			default:
				isReal = false;
				System.out.println("Keine r�mische Zahl eingegeben.");
				break;
			}
		}



		if (isReal && !error) {
			System.out.println("Die r�mische Zahl " + roemisch + " entspricht der Dezimalzahl " + decimal + ".");
		} else {
			System.out.println("Ung�ltig");
		}

		scanner.close();
	}

}
