import java.util.Scanner;
public class Addition {
	public static void main(String[]args) {
		double erg=0.0,zahl1=0.0,zahl2=0.0;
		programmhinweis();
		zahl1=eingabe("1. Zahl: ");
		zahl2=eingabe("2. Zahl: ");
		erg=verarbeitung(zahl1,zahl2);
		ausgabe(erg,zahl1,zahl2);	
	}
	public static void programmhinweis() {
        System.out.println("Hinweis: ");
        System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
    }
	public static void ausgabe(double erg,double zahl1,double zahl2) {
        System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f = %.2f + %.2f", erg, zahl1, zahl2);
    }
	public static double verarbeitung(double zahl1,double zahl2) {
		double ergebnis;
		ergebnis=zahl1+zahl2;
		return ergebnis;
	}
	public static double eingabe(String text) {
		Scanner sc=new Scanner(System.in);
		System.out.println(text);
		double zahl=sc.nextDouble();
		return zahl;
	}

}